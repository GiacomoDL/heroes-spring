package heroes;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/hero")
public class Controller {
    @Autowired
    private HeroRepository heroRepository;
    @Autowired
    private AbilityRepository abilityRepository;

//    @PostConstruct
//    public void addAbility() {
//        List<Ability> listAbility = new ArrayList<>();
//        List<Ability> listAbility1 = new ArrayList<>();
//        List<Ability> listAbility2 = new ArrayList<>();
//        List<Ability> listAbility3 = new ArrayList<>();
//        List<Ability> listAbility4 = new ArrayList<>();
//
//        Ability ability1 = new Ability("Volare", "Nel blu dipinto di blu");
//        Ability ability2 = new Ability("Super Forza", "Utile contro i barattoli più impegnativi");
//        Ability ability3 = new Ability("Raggi XYZ", "Può vedere attraverso le cose eheh");
//        Ability ability4 = new Ability("Telecinesi", "Rigorosamente made in China");
//        listAbility.add(ability1);
//        listAbility.add(ability2);
//        listAbility.add(ability3);
//        listAbility.add(ability4);
//        listAbility1.add(ability1);
//        listAbility2.add(ability4);
//        listAbility3.add(ability2);
//        listAbility4.add(ability1);
//        listAbility4.add(ability2);
//        listAbility4.add(ability3);
//        for (Ability ability : listAbility) {
//            abilityRepository.save(ability);
//        }
//        Hero hero = new Hero("Uomo Volante", "volare@ohoh.it", listAbility1);
//        Hero hero2 = new Hero("Xavier", "xavier@xmen.us", listAbility2);
//        Hero hero3 = new Hero("Hulk", "hulk@marvel.us", listAbility3);
//        Hero hero4 = new Hero("Superman", "superman@dc.us", listAbility4);
//        heroRepository.save(hero);
//        heroRepository.save(hero2);
//        heroRepository.save(hero3);
//        heroRepository.save(hero4);
//    }

    private boolean checkEmail(String email) {
        if (email == null || email.equals("")) {
            return true;
        } else {
            return EmailValidator.getInstance().isValid(email);
        }
    }

    private boolean userNotExist(Hero hero) {
        return getByName(hero.getName()).getStatusCode().is4xxClientError();
    }

    private boolean abilityNotExist(Ability ability) {
        return getAbilityByName(ability.getName()).getStatusCode().is4xxClientError();
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    @ResponseBody
    public ResponseEntity addHero(@RequestBody Hero hero) {
        try {
            if (!userNotExist(hero)) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            if (checkEmail(hero.getEmail())) {
                heroRepository.save(hero);
                return new ResponseEntity(HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (TransactionSystemException | NullPointerException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @GetMapping(path = "/all")
    public ResponseEntity getAllHeroes() {
        try {
            Iterable<Hero> listHero = heroRepository.findAll();
            return new ResponseEntity(listHero, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @GetMapping(path = "/find/{id}")
    public ResponseEntity getById(@PathVariable int id) {
        try {
            Hero hero = heroRepository.findById(id);
            if (hero == null)
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            return new ResponseEntity(hero, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @GetMapping(path = "/search/{name}")
    public ResponseEntity getByName(@PathVariable String name) {
        try {
            Hero hero = heroRepository.findByName(name);
            if (hero == null)
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            return new ResponseEntity(hero, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @PostMapping(path = "/updateEmail")
    @ResponseBody
    public ResponseEntity updateEmail(@RequestBody Hero heroEmail) {
        try {
            if (checkEmail(heroEmail.getEmail())) {
                Hero hero = heroRepository.findById(heroEmail.getId());
                hero.setEmail(heroEmail.getEmail());
                heroRepository.save(hero);
                return new ResponseEntity(HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    @ResponseBody
    public ResponseEntity deleteHero(@PathVariable int id) {
        try {
            Hero hero = heroRepository.findById(id);
            heroRepository.delete(hero);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


    @CrossOrigin
    @DeleteMapping(path = "/deleteAbility/{idHero}/{idAbility}")
    @ResponseBody
    public ResponseEntity deleteAbility(@PathVariable int idHero, @PathVariable int idAbility) {
        try {
            List<Ability> listAbility = new ArrayList<>();
            Hero hero = heroRepository.findById(idHero);
            Ability ability = abilityRepository.findById(idAbility);
            for (Ability abilityFor : hero.getListAbility()) {
                if (abilityFor.getId() != ability.getId()) {
                    listAbility.add(abilityFor);
                }
            }
            hero.setListAbility(listAbility);
            heroRepository.save(hero);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @GetMapping(path = "/allAbility")
    public ResponseEntity getAllAbility() {
        try {
            Iterable<Ability> listAbility = abilityRepository.findAll();
            return new ResponseEntity(listAbility, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @PostMapping(path = "/addAbilityToHero/{idHero}/{idAbility}")
    @ResponseBody
    public ResponseEntity addAbilityToHero(@PathVariable int idHero, @PathVariable int idAbility) {
        try {
            List<Ability> listAbility;
            Hero hero = heroRepository.findById(idHero);
            Ability ability = abilityRepository.findById(idAbility);
            listAbility = hero.getListAbility();
            listAbility.add(ability);
            hero.setListAbility(listAbility);
            heroRepository.save(hero);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @PostMapping(path = "/addAbilityToDb/{idHero}")
    @ResponseBody
    public ResponseEntity addAbilityToDb(@RequestBody Ability ability, @PathVariable int idHero) {
        try {
            if (!abilityNotExist(ability)) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            if (ability.getName().isEmpty() || ability.getDescription().isEmpty())
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            Hero hero = heroRepository.findById(idHero);
            List<Ability> listAbility = hero.getListAbility();
            listAbility.add(ability);
            abilityRepository.save(ability);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @DeleteMapping(path = "/deleteAbilityFromDb/{id}")
    @ResponseBody
    public ResponseEntity deleteAbilityFromDb(@PathVariable int id) {
        try {
            Ability ability = abilityRepository.findById(id);
            abilityRepository.delete(ability);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @GetMapping(path = "/searchAbility/{name}")
    public ResponseEntity getAbilityByName(@PathVariable String name) {
        try {
            Ability ability = abilityRepository.findByName(name);
            if (ability == null)
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            return new ResponseEntity(ability, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }
}
