package heroes;
import org.springframework.data.repository.CrudRepository;

public interface HeroRepository extends CrudRepository<Hero, Integer> {
    Hero findById(int id);
    Hero findByName(String name);
}