package heroes;

import org.springframework.data.repository.CrudRepository;

public interface AbilityRepository extends CrudRepository<Ability, Integer> {
    Ability findById(int id);
    Ability findByName(String name);
}
