package heroes;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Hero {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotNull
    @Size(min = 3)
    private String name;
    private String email;
    @ManyToMany
    @JoinTable(
            name = "HERO_ABI",
            joinColumns = @JoinColumn(name = "HERO_ID", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "ABI_ID", referencedColumnName = "id"))
   private List<Ability> listAbility;

    public Hero() {

    }

    public Hero(String name, String email, List<Ability> listAbility) {
        this.name = name;
        this.email = email;
        this.listAbility = listAbility;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Ability> getListAbility() {
        return listAbility;
    }

    public void setListAbility(List<Ability> listAbility) {
        this.listAbility = listAbility;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", listAbility='" + listAbility + '\'' +
                '}';
    }
}
